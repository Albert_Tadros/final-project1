/* compute optimal solutions for sliding block puzzle. */
#include <iostream>
using std::cout;
using std::endl;
#include <vector>
using std::vector;
#include <SDL2/SDL.h>
#include <stdio.h>
#include <cstdlib>   /* for atexit() */
#include <algorithm>
using std::swap;

#include <cassert>

/* SDL reference: https://wiki.libsdl.org/CategoryAPI */

/* initial size; will be set to screen size after window creation. */
int SCREEN_WIDTH = 640;
int SCREEN_HEIGHT = 480;
int fcount = 0;
int mousestate = 0;
SDL_Point lastm = {0,0}; /* last mouse coords */
SDL_Rect bframe; /* bounding rectangle of board */
static const int ep = 2; /* epsilon offset from grid lines */

bool init(); /* setup SDL */
void initBlocks();

#define FULLSCREEN_FLAG SDL_WINDOW_FULLSCREEN_DESKTOP
// #define FULLSCREEN_FLAG 0

/* NOTE: ssq == "small square", lsq == "large square" */
enum bType {hor = 0,ver= 1,ssq = 2,lsq =  3};
struct block {
	SDL_Rect R; /* screen coords + dimensions */
	bType type; /* shape + orientation */
	int v;  // for indexing
	block *edge; // address of each block

	void rotate() /* rotate rectangular pieces */
	{
		if (type != hor && type != ver) return;
		type = (type==hor)?ver:hor;
		swap(R.w,R.h);
	}
};


#define NBLOCKS 10
block B[NBLOCKS];
block* dragged = NULL;

block* findBlock(int x, int y);
void close(); /* call this at end of main loop to free SDL resources */
SDL_Window* gWindow = 0; /* main window */
SDL_Renderer* gRenderer = 0;

bool init()
{
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		printf("SDL_Init failed.  Error: %s\n", SDL_GetError());
		return false;
	}
	/* NOTE: take this out if you have issues, say in a virtualized
	 * environment: */
	if(!SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1")) {
		printf("Warning: vsync hint didn't work.\n");
	}
	/* create main window */
	gWindow = SDL_CreateWindow("Sliding block puzzle solver",
								SDL_WINDOWPOS_UNDEFINED,
								SDL_WINDOWPOS_UNDEFINED,
								SCREEN_WIDTH, SCREEN_HEIGHT,
								SDL_WINDOW_SHOWN|FULLSCREEN_FLAG);
	if(!gWindow) {
		printf("Failed to create main window. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	/* set width and height */
	SDL_GetWindowSize(gWindow, &SCREEN_WIDTH, &SCREEN_HEIGHT);
	/* setup renderer with frame-sync'd drawing: */
	gRenderer = SDL_CreateRenderer(gWindow, -1,
			SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(!gRenderer) {
		printf("Failed to create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}
	SDL_SetRenderDrawBlendMode(gRenderer,SDL_BLENDMODE_BLEND);

	initBlocks();
	return true;
}

/* TODO: you'll probably want a function that takes a state / configuration
 * and arranges the blocks in accord.  This will be useful for stepping
 * through a solution.  Be careful to ensure your underlying representation
 * stays in sync with what's drawn on the screen... */


class solve {
	public:

		vector<int> child;	// vector to store a state after block movment
		vector<int> parent;	// vector to store the state before child
		int col = 5;  		/* used to make constraint since a 1D array is used to
													represent states of the board */

		void MvRight(vector<int> state, int i)
		{
			vector<int> arr;
			arr = state;

			if(arr[i] != -1){
				i++;
				MvRight(state,i);
			}
			else{

				if (arr[i] == -1 && arr[i+1] != -1){
					int a = arr[i + 1];
					arr[i + 1] = arr[i];
					arr[i] = a;
				}
				else{

					if(arr[i] == -1 && arr[i+1] == -1){
						int b = arr[i + 2];
						arr[i + 2] = arr[i + 1];
						arr[i + 1] = b;
						int a = arr[i + 1];
						arr[i + 1] = arr[i];
						arr[i] = a;
					}
				}
				for (int i = 0; i < 20; i++){
					cout << arr[i] << " ";
				}
				cout << endl;
			}
		}

		void MvLeft(vector<int> state, int i)
		{
			vector<int> arr;
			arr = state;

			if(arr[i] != -1){
				i++;
				MvLeft(state,i);
			}
			else{

				if (arr[i] == -1 && arr[i+1] != -1){
					int a = arr[i - 1];
					arr[i - 1] = arr[i];
					arr[i] = a;
				}
				else{

					if(arr[i] == -1 && arr[i+1] == -1){
						int b = arr[i - 1];
						arr[i - 1] = arr[i];
						arr[i] = b;
						int c = arr[i];
						arr[i] = arr[i+1];
						arr[i+1] = c;
					}
				}

				for (int i = 0; i < 20; i++){
					cout << arr[i] << " ";
				}
				cout << endl;
			}
		}
	};
	// implemeting an bfs to compare a given state with the goal state and solve the puzzle. if state != GoalState, then a movement of blocks should be done while saving each state and printing it on the screen.



void initBlocks()
{
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int h = H*3/4;
	int w = 4*h/5;
	int u = h/5-2*ep;
	int mw = (W-w)/2;
	int mh = (H-h)/2;

	/* setup bounding rectangle of the board: */
	bframe.x = (W-w)/2;
	bframe.y = (H-h)/2;
	bframe.w = w;
	bframe.h = h;

	/* NOTE: there is a tacit assumption that should probably be
	 * made explicit: blocks 0--4 are the rectangles, 5-8 are small
	 * squares, and 9 is the big square.  This is assumed by the
	 * drawBlocks function below. */

	for (size_t i = 0; i < 5; i++) {
		B[i].R.x = (mw-2*u)/2;
		B[i].R.y = mh + (i+1)*(u/5) + i*u;
		B[i].R.w = 2*(u+ep);
		B[i].R.h = u;
		B[i].type = hor;
	}
	B[4].R.x = mw+ep;
	B[4].R.y = mh+ep;
	B[4].R.w = 2*(u+ep);
	B[4].R.h = u;
	B[4].type = hor;
	/* small squares */
	for (size_t i = 0; i < 4; i++) {
		B[i+5].R.x = (W+w)/2 + (mw-2*u)/2 + (i%2)*(u+u/5);
		B[i+5].R.y = mh + ((i/2)+1)*(u/5) + (i/2)*u;
		B[i+5].R.w = u;
		B[i+5].R.h = u;
		B[i+5].type = ssq;
	}
	B[9].R.x = B[5].R.x + u/10;
	B[9].R.y = B[7].R.y + u + 2*u/5;
	B[9].R.w = 2*(u+ep);
	B[9].R.h = 2*(u+ep);
	B[9].type = lsq;
}

void drawBlocks()
{
	/* rectangles */
	SDL_SetRenderDrawColor(gRenderer, 0x43, 0x4c, 0x5e, 0xff);
	for (size_t i = 0; i < 5; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* small squares */
	SDL_SetRenderDrawColor(gRenderer, 0x5e, 0x81, 0xac, 0xff);
	for (size_t i = 5; i < 9; i++) {
		SDL_RenderFillRect(gRenderer,&B[i].R);
	}
	/* large square */
	SDL_SetRenderDrawColor(gRenderer, 0xa3, 0xbe, 0x8c, 0xff);
	SDL_RenderFillRect(gRenderer,&B[9].R);
}

/* return a block containing (x,y), or NULL if none exists. */
block* findBlock(int x, int y)
{
	/* NOTE: we go backwards to be compatible with z-order */
	for (int i = NBLOCKS-1; i >= 0; i--) {
		if (B[i].R.x <= x && x <= B[i].R.x + B[i].R.w &&
				B[i].R.y <= y && y <= B[i].R.y + B[i].R.h)
			return (B+i);
	}
	return NULL;
}

void close()
{
	SDL_DestroyRenderer(gRenderer); gRenderer = NULL;
	SDL_DestroyWindow(gWindow); gWindow = NULL;
	SDL_Quit();
}

void render()
{
	/* draw entire screen to be black: */
	SDL_SetRenderDrawColor(gRenderer, 0x00, 0x00, 0x00, 0xff);
	SDL_RenderClear(gRenderer);

	/* first, draw the frame: */
	int& W = SCREEN_WIDTH;
	int& H = SCREEN_HEIGHT;
	int w = bframe.w;
	int h = bframe.h;
	SDL_SetRenderDrawColor(gRenderer, 0x39, 0x39, 0x39, 0xff);
	SDL_RenderDrawRect(gRenderer, &bframe);
	/* make a double frame */
	SDL_Rect rframe(bframe);
	int e = 3;
	rframe.x -= e;
	rframe.y -= e;
	rframe.w += 2*e;
	rframe.h += 2*e;
	SDL_RenderDrawRect(gRenderer, &rframe);

	/* draw some grid lines: */
	SDL_Point p1,p2;
	SDL_SetRenderDrawColor(gRenderer, 0x19, 0x19, 0x1a, 0xff);
	/* vertical */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x;
	p2.y = p1.y + h;
	for (size_t i = 1; i < 4; i++) {
		p1.x += w/4;
		p2.x += w/4;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	/* horizontal */
	p1.x = (W-w)/2;
	p1.y = (H-h)/2;
	p2.x = p1.x + w;
	p2.y = p1.y;
	for (size_t i = 1; i < 5; i++) {
		p1.y += h/5;
		p2.y += h/5;
		SDL_RenderDrawLine(gRenderer,p1.x,p1.y,p2.x,p2.y);
	}
	SDL_SetRenderDrawColor(gRenderer, 0xd8, 0xde, 0xe9, 0x7f);
	SDL_Rect goal = {bframe.x + w/4 + ep, bframe.y + 3*h/5 + ep,
	                 w/2 - 2*ep, 2*h/5 - 2*ep};
	SDL_RenderDrawRect(gRenderer,&goal);

	/* now iterate through and draw the blocks */
	drawBlocks();
	/* finally render contents on screen, which should happen once every
	 * vsync for the display */
	SDL_RenderPresent(gRenderer);
}

void snap(block* b)
{
	/* TODO: once you have established a representation for configurations,
	 * you should update this function to make sure the configuration is
	 * updated when blocks are placed on the board, or taken off.  */
	assert(b != NULL);
	/* upper left of grid element (i,j) will be at
	 * bframe.{x,y} + (j*bframe.w/4,i*bframe.h/5) */
	/* translate the corner of the bounding box of the board to (0,0). */
	int x = b->R.x - bframe.x;
	int y = b->R.y - bframe.y;
	int uw = bframe.w/4;
	int uh = bframe.h/5;
	/* NOTE: in a perfect world, the above would be equal. */
	int i = (y+uh/2)/uh; /* row */
	int j = (x+uw/2)/uw; /* col */
	if (0 <= i && i < 5 && 0 <= j && j < 4) {
		b->R.x = bframe.x + j*uw + ep;
		b->R.y = bframe.y + i*uh + ep;
	}
}


	void SetInitConfig() /*
the function that sets the intial configuartion of the blocks */
{
	int Ww = bframe.w/4; // x-cursor postion
	int Hh = bframe.h/5; // y-cursor position
	// moving rectangles into the board   
	// rect 1   
	B[0].R.x = bframe.x + 0*Ww + ep;
	B[0].R.y = bframe.y + 0*Hh + ep;
	B[0].rotate();
	B[0].v = 0;

	// rect 2   
	B[1].R.x = bframe.x + 3*Ww + ep;
	B[1].R.y = bframe.y + 0*Hh + ep;
	B[1].rotate();
	B[1].v = 1;

		// rect 3   
	B[2].R.x = bframe.x + 0*Ww + ep;
	B[2].R.y = bframe.y + 3*Hh + ep;
	B[2].rotate();
	B[2].v = 2;
		// rec 4   
	B[3].R.x = bframe.x + 2*Ww + ep;
	B[3].R.y = bframe.y + 3*Hh + ep;
	B[3].rotate();
	B[3].v = 3;

		// rect 5  
	B[4].R.x = bframe.x + 1*Ww + ep;
	B[4].R.y = bframe.y + 2*Hh + ep;
	B[4].v = 4;

	// moving squares into the board 
		// square 1
	B[5].R.x = bframe.x + 1*Ww + ep;
	B[5].R.y = bframe.y + 3*Hh + ep;
	B[5].v = 5;

		// square 2  
	B[6].R.x = bframe.x + 3*Ww + ep;
	B[6].R.y = bframe.y + 3*Hh + ep;
	B[6].v = 6;

		// square 3   
	B[7].R.x = bframe.x + 1*Ww + ep;
	B[7].R.y = bframe.y + 4*Hh + ep;
	B[7].v = 7;

		// square 4 
	B[8].R.x = bframe.x + 3*Ww + ep;
	B[8].R.y = bframe.y + 4*Hh + ep;
	B[8].v = 8;

		// large square  
	B[9].R.x = bframe.x + 1*Ww + ep;
	B[9].R.y = bframe.y + 0*Hh + ep;
	B[9].v = 9;
	}

/* creating an avector of int to represent the board of intial cofig.
	value -1 is considered the space in the board (an empty ssq)*/

vector<int> board =
	{
		ver, lsq, lsq, ver,
		ver, lsq, lsq, ver,
		-1, hor, hor , -1,
		hor, ssq, hor, ssq,
		hor, ssq, hor, ssq
	};

/* creating an array of long int to represent the goal state(solution).
	Each state stepping through the GoalState will be compared to GoalState
	until it's reached (solution reached).
	value -1 is considered the space in the board (an empty ssq).*/
vector<int> GoalState =
	{
		ver, hor, hor, ver,
		ver, ver, ssq, ver,
		ssq, ver, hor, hor,
		-1, lsq, lsq, ssq,
		-1, lsq, lsq, ssq
	};





void printstate(vector<int> array)

{
	for (int i = 0; i < 20; i++){
			cout << array[i] << " ";
		};
		cout <<endl;
	}


int main(int argc, char *argv[])
{
	/* TODO: add option to specify starting state from cmd line? */
	/* start SDL; create window and such: */
	if(!init()) {
		printf( "Failed to initialize from main().\n" );
		return 1;
	}



	SetInitConfig();
	printstate(board);
	solve mysolve;
	mysolve.MvRight(board,0); // for testing the movement to right
	mysolve.MvLeft(board,0);	// for testing the movement to left



	atexit(close);
	bool quit = false; /* set this to exit main loop. */
	SDL_Event e;
	/* main loop: */
	while(!quit) {
		/* handle events */
		while(SDL_PollEvent(&e) != 0) {
			/* meta-q in i3, for example: */
			if(e.type == SDL_MOUSEMOTION) {
				if (mousestate == 1 && dragged) {
					int dx = e.button.x - lastm.x;
					int dy = e.button.y - lastm.y;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged->R.x += dx;
					dragged->R.y += dy;
				}
			} else if (e.type == SDL_MOUSEBUTTONDOWN) {
				if (e.button.button == SDL_BUTTON_RIGHT) {
					block* b = findBlock(e.button.x,e.button.y);
					if (b) b->rotate();
				} else {
					mousestate = 1;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					dragged = findBlock(e.button.x,e.button.y);
				}
				/* XXX happens if during a drag, someone presses yet
				 * another mouse button??  Probably we should ignore it. */
			} else if (e.type == SDL_MOUSEBUTTONUP) {
				if (e.button.button == SDL_BUTTON_LEFT) {
					mousestate = 0;
					lastm.x = e.button.x;
					lastm.y = e.button.y;
					if (dragged) {
						/* snap to grid if nearest location is empty. */
						snap(dragged);
					}
					dragged = NULL;
				}
			} else if (e.type == SDL_QUIT) {
				quit = true;
			} else if (e.type == SDL_KEYDOWN) {
				switch (e.key.keysym.sym) {
					case SDLK_ESCAPE:
					case SDLK_q:
						quit = true;
						break;
					case SDLK_LEFT:
						/* TODO: show previous step of solution */
						break;
					case SDLK_RIGHT:
						/* TODO: show next step of solution */
						break;
					case SDLK_p:
						/* TODO: print the state to stdout
						 * (maybe for debugging purposes...) */
						break;
					case SDLK_s:
						/* TODO: try to find a solution */
						break;
					default:
						break;
				}
			}
		}
		fcount++;
		render();
	}

	printf("total frames rendered: %i\n",fcount);
	return 0;
}
